<?php

ob_start();

define( 'BASE_PATH', __DIR__ . '/..' );
define( 'LIBRARY_PATH', __DIR__ . '/../library' );
define( 'HTML_TEMPLATE_PATH', __DIR__ . '/template' );

require LIBRARY_PATH . '/autoload.php';

$app = new Admin();
$app->go();