<?php

/**
 * Class Application
 */
class Application
{
    const ANSWER_YES = 'yes';
    const ANSWER_NO  = 'no';

    /**
     * Database control
     *
     * @var Model
     */
    protected $_model;

    /**
     * Filters email address for email headers
     *
     * @param   string  $email  Email address
     *
     * @return  string
     */
    protected function _filterEmail($email)
    {
        $rule = [
            "\r" => '',
            "\n" => '',
            "\t" => '',
            '"'  => '',
            ','  => '',
            '<'  => '',
            '>'  => '',
        ];

        return strtr($email, $rule);
    }

    /**
     * Filters name for email headers
     *
     * @param   string  $name   Name before email, e.g. "John Doe <smth@smth.com>"
     *
     * @return  string
     */
    protected function _filterName($name)
    {
        $rule = [
            "\r" => '',
            "\n" => '',
            "\t" => '',
            '"'  => "'",
            '<'  => '[',
            '>'  => ']',
        ];

        return trim(strtr($name, $rule));
    }

    /**
     * Filters other data for email
     *
     * @param   string  $data   Random string
     *
     * @return  string
     */
    protected function _filterOther($data)
    {
        $rule = [
            "\r" => '',
            "\n" => '',
            "\t" => '',
        ];

        return strtr($data, $rule);
    }

    /**
     * Initializes the application
     */
    protected function _init()
    {
        $developmentMode = \Configuration::getParameter( 'developmentMode' );
        $showErrors  = \Configuration::getParameter( 'showPhpErrors' );
        if ( $developmentMode && $showErrors ) {
            ini_set( 'error_reporting', E_ALL );
            ini_set( 'display_startup_errors', 1 );
            ini_set( 'html_errors', 1 );
            ini_set( 'display_errors', 1 );
        }
    }

    /**
     * Returns new generated token
     * 
     * @return  string
     */
    protected function _getCsrfToken()
    {
        $firstPart = md5( date( 'Y:m:d:i:s' ) );
        $secondPart = md5( mt_rand( 0, 999999) );

        $randomPosition = mt_rand( 0, 31 ); // where to split

        $token = str_shuffle( substr( $firstPart, 0, $randomPosition ) . substr( $secondPart, $randomPosition ) );

        return $token;
    }

    /**
     * Returns if token is the same as in the session
     * 
     * @return  bool
     */
    protected function _checkCsrfToken()
    {
        $sessionToken = \Requester::getSessionVariable( 'csrfToken' );
        $postToken = \Requester::get( 'csrf_token' );

        if ( $sessionToken && $sessionToken == $postToken ) {
            return true;
        }

        return false;
    }

    /**
     * AJAX request
     * 
     * @return  bool
     * 
     * @throws  Exception
     */
    protected function _ajax()
    {
        if ( !$this->_checkCsrfToken() ) {
            throw new Exception( 'CSRF token is invalid' );
        }

        $answers = Requester::get( 'answers', [] );

        $name = Requester::get( 'name' );
        $rawEmail = Requester::get( 'email' );
        $position = Requester::get( 'position' );

        $requestId = $this->_model->saveRequest( $name, $rawEmail, $position, json_encode( $answers ) );

        $fromName = $this->_filterName( Configuration::getParameter('emailFromName') );
        $name = $this->_filterName( $name );
        $email = $this->_filterEmail( $rawEmail );
        //$position = $this->_filterName( $position );

        $validregexp = '/^([a-z0-9\+_\-\.]+)(\.[a-z0-9\+_\-\.]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix';
        $isEmailValid = filter_var(
            $email,
            \FILTER_VALIDATE_REGEXP,
            array( 'options' => array( 'regexp' => $validregexp ) )
        );
        if ( !$isEmailValid ) {
            throw new \Exception ('Email address must valid: ' . $rawEmail);
        }

        $emailFrom = Configuration::getParameter('emailFrom');
        $isEmailValid = filter_var(
            $emailFrom,
            \FILTER_VALIDATE_REGEXP,
            array( 'options' => array( 'regexp' => $validregexp ) )
        );
        if ( !$isEmailValid ) {
            throw new \Exception ('"Email from" address must valid.');
        }

        $headersList = [
            'Content-type: text/html; charset=utf-8',
            'From: =?UTF-8?B?' . base64_encode( $fromName ) . '?= <' . $emailFrom . '>',
        ];
        if ( $name ) {
            $email = '=?UTF-8?B?' . base64_encode( $name ) . '?= <' . $email . '>';
        }
        $emailSubject = $this->_filterOther( \Configuration::getParameter( 'emailSubject', 'Security' ) );
        $headers = implode("\r\n", $headersList);

        $message = $this->_prepareEmail( $answers, $name );

        $success = mail( $email, $emailSubject, $message, $headers );

        $this->_model->setRequestSuccess( $requestId, $success );

        return $success;
    }

    protected function _prepareEmail( $answers, $name )
    {
        $emailTplPath = EMAIL_TEMPLATE_PATH . '/template.tpl';
        $answerTplPath = EMAIL_TEMPLATE_PATH . '/_answer.tpl';

        if ( !file_exists( $emailTplPath ) || !file_exists( $answerTplPath ) ) {
            throw new Exception('Email template was not found');
        }

        $emailTpl = file_get_contents( $emailTplPath );
        $answerTpl = file_get_contents( $answerTplPath );

        $questions = $this->_model->loadQuestions();

        $answersHtml = '';

        foreach ( $questions as $number => $question ) {
            $answer = empty($answers[$number]) || $answers[$number] != self::ANSWER_YES
                ? self::ANSWER_NO
                : self::ANSWER_YES;

            $answerInTpl = $answer == self::ANSWER_YES ? 'Да' : 'Нет';
            $decision = $answer == self::ANSWER_YES ? $question->decision_yes : $question->decision_no;

            $data = [
                '{question}' => trim( $question->question ),
                '{answer}' => trim( $answerInTpl ),
                '{decision}' => trim( $decision ),
            ];

            $answersHtml .= strtr( $answerTpl, $data );
        }

        $message = strtr(
            $emailTpl,
            [ '{name}' => htmlspecialchars( $name ), '{answers}' => $answersHtml ]
        );

        return $message;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        \Configuration::init();

        $this->_init();

        $this->_model = new Model();
    }

    /**
     * Runs the application
     */
    public function go()
    {
        $ajax = Requester::get( 'ajax' );

        if ( $ajax ) {
            try {
                if ( !$this->_ajax() ) {
                    // Exceptions are everywhere
                    throw new Exception( 'Email sending failed' );
                }
                http_response_code(200);
            } catch (Exception $e) {
                error_log($e->getMessage());
                http_response_code(500);
                //header("HTTP/1.1 500 Internal Server Error");
            }
            ob_clean();
            exit;
        } else {
            $csrfToken = $this->_getCsrfToken();
            \Requester::setSessionVariable( 'csrfToken', $csrfToken );
//            $questions = $this->_model->loadQuestions();
//            $questionsCount = count( $questions );
            require HTML_TEMPLATE_PATH . '/index.phtml';
        }
    }

}