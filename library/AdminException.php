<?php

/**
 * Class AdminException
 */
class AdminException extends Exception
{
    /**
     * URL to redirect after exception has happened
     * 
     * @var string
     */
    protected $_urlToRedirect;

    /**
     * Returns redirect URL
     * 
     * @return  string
     */
    public function getUrlToRedirect()
    {
        return $this->_urlToRedirect;
    }

    /**
     * Sets redirect URL
     * 
     * @param   string  $url    URL
     * 
     * @return  $this
     */
    public function setUrlToRedirect($url)
    {
        $this->_urlToRedirect = $url;
        
        return $this;
    }

    /**
     * AdminException constructor.
     * 
     * @param   string          $message    Exception message
     * @param   int             $code       Error code
     * @param   Exception|null  $previous   Previous exception
     */
    public function __construct( $message = "", $code = 0, Exception $previous = null )
    {
        parent::__construct( $message, $code, $previous );
    }
}