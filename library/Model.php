<?php

/**
 * Class Model
 */
class Model
{
    /**
     * Loads all questions
     *
     * @return array
     */
    public function loadQuestions()
    {
        $db = \Db::getInstance();

        $query = "SELECT * FROM `questions` ORDER BY `order_number` ASC";

        $result = [];
        $queryResult = $db->execute( $query );
        $i = 0;
        while ( $row = $queryResult->fetch_object() ) {
            $result[++$i] = $row;
        }
        
        return $result;
    }

    /**
     * Loads question by id
     *
     * @param   int $id Question id
     *
     * @return  stdClass
     */
    public function loadQuestion( $id )
    {
        $db = \Db::getInstance();

        $query = "SELECT * FROM `questions` WHERE `id` = '" . $db->escape( $id ) . "';";

        $queryResult = $db->execute( $query );

        return $queryResult->fetch_object();
    }

    /**
     * Removes question from the database
     *
     * @param   int $id Question id
     *
     * @return  bool
     */
    public function deleteQuestion( $id )
    {
        if ( !$id ) {
            return false;
        }

        $db = \Db::getInstance();

        $query = "DELETE FROM `questions` WHERE `id` = '" . $db->escape( $id ) . "';";

        return (bool)$db->execute( $query );
    }

    /**
     * Saves question (new or already existing)
     *
     * @param   string      $question       Question text
     * @param   string      $description    Question description
     * @param   string      $number         Question number
     * @param   string      $decisionYes    Decision text if answer is yes
     * @param   string      $decisionNo     Decision text if answer is no
     * @param   int|null    $id             Question id, in case it is existing question
     *
     * @return  bool
     */
    public function saveQuestion( $question, $description, $number, $decisionYes, $decisionNo, $id = null )
    {
        $db = \Db::getInstance();

        $query = " `questions` SET `question` = '" . $db->escape( $question ) . "'"
                . ", `description` = '"  . $db->escape( $description ) . "'"
                . ", `order_number` = '" . $db->escape( $number )      . "'"
                . ", `decision_yes` = '" . $db->escape( $decisionYes ) . "'"
                . ", `decision_no` = '"  . $db->escape( $decisionNo )  . "'";
        
        if ( $id ) {
            $query = "UPDATE" . $query . " WHERE `id` = '" . $db->escape( $id ) . "';";
        } else {
            $query = "INSERT INTO" . $query . ";";
        }

        return (bool)$db->execute( $query );
    }

    /**
     * Is question number already using or not
     *
     * @param   int $id     Question id
     * @param   int $number Question number
     *
     * @return  bool
     */
    public function isQuestionNumberUsing( $id, $number )
    {
        $db = \Db::getInstance();

        $query = "SELECT * FROM `questions` WHERE `order_number` = '" . $db->escape( $number ) . "'";
        $query .= ( $id ? " AND `id` != '" . $db->escape( $id ) . "'" : '' );

        return (bool)$db->execute( $query )->num_rows;
    }

    /**
     * Saves new request. Returns id of the new request
     * 
     * @param   string  $name       Name field value
     * @param   string  $email      Email field value
     * @param   string  $position   Position field value
     * @param   string  $answers    JSON decoded answers 
     * 
     * @return  int
     */
    public function saveRequest( $name, $email, $position, $answers )
    {
        $db = \Db::getInstance();

        $query = "INSERT INTO `requests` SET `name`='" . $db->escape( $name ) . "'"
            . ", `email`='" . $db->escape( $email ) . "', `position`='" . $db->escape( $position ) . "'"
            . ", `answers`='" . $db->escape( $answers ) . "'";

        $db->execute( $query );

        return $db->getDriver()
            ->getMysqli()
            ->insert_id;
    }

    /**
     * Sets request "success" field
     * 
     * @param   int     $requestId  Request id
     * @param   bool    $success    Success
     * 
     * @return  bool
     */
    public function setRequestSuccess( $requestId, $success )
    {
        $db = \Db::getInstance();

        $query = "UPDATE `requests` SET `success`='" . (int)(bool)$success . "'" 
            . " WHERE `id`='" . $db->escape( $requestId ) . "'";

        return (bool)$db->execute( $query );
    }

    /**
     * Loads requests
     * 
     * @param   int $limit  Limit
     * @param   int $offset Offset of the limit
     * 
     * @return  stdClass
     */
    public function loadRequests( $limit = null, $offset = null )
    {
        $db = \Db::getInstance();

        $query = "SELECT * FROM `requests` ORDER BY `added_on` DESC, `id` DESC";
            if ( $limit !== null && $limit !== false && $limit !== '' ) {
                $query .= " LIMIT ";
                if ( $offset !== null && $offset !== false && $offset !== '' ) {
                    $query .= (int)$offset . ', ';
                }
                $query .= (int)$limit;
            }

        $requests = [];
        $queryResult = $db->execute( $query );
        $i = 0;
        while ( $row = $queryResult->fetch_object() ) {
            $requests[++$i] = $row;
        }

        $result = new stdClass();
        $result->requests = $requests;
        $result->count = 1;

        $queryResult = $db->execute( "SELECT COUNT(*) AS num_rows FROM `requests`" );
        if ( $queryResult ) {
            $result->count = (int)$queryResult->fetch_object()->num_rows;
        }

        return $result;
    }
    
}