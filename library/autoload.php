<?php

/**
 * Autoload function
 *
 * @param   string  $className  Full (with the namespaces) name of the class
 *
 * @throws  Exception   If file can't be found
 *
 * @return  string
 */
spl_autoload_register( function( $className ) {
    $path = __DIR__ . '/' . $className . '.php';

    if ( file_exists( $path ) ) {
        include $path;

        return true;
    }

    return false;
});
