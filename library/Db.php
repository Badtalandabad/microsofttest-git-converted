<?php

/**
 * Class Db
 * Database control class. Singletone.
 *
 * @package Library\Db
 */
class Db
{
    /**
     * Single instance of the object
     *
     * @var self
     */
    private static $instance;

    /**
     * Database driver object
     *
     * @var MysqliDriver
     */
    private $_driver;

    /**
     * Returns single instance of the object
     *
     * @return  Db
     */
    public static function getInstance()
    {
        if ( empty( self::$instance ) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Closed methods
     */
    private function __clone(){}
    private function __wakeup(){}

    /**
     * Constructor
     */
    protected function __construct()
    {
        $dbDriverName = \Configuration::getParameter( 'dbDriver' );

        $this->connect( $dbDriverName );
    }

    /**
     * Sets database driver object
     *
     * @param   MysqliDriver    $driver Database driver object
     *
     * @return  $this
     */
    public function setDriver( $driver )
    {
        $this->_driver = $driver;

        return $this;
    }

    /**
     * Returns database driver object
     *
     * @return  MysqliDriver
     */
    public function getDriver()
    {
        return $this->_driver;
    }

    /**
     * Sets quote symbol. Which using in quote() and q() methods
     *
     * @param   string  $quote  Quote symbol
     *
     * @return  $this
     */
    public function setQuote( $quote )
    {
        $this->getDriver()
            ->setQuote( $quote );

        return $this;
    }

    /**
     * Returns quote symbol. Which using in quote() and q() methods
     *
     * @return  string
     */
    public function getQuote()
    {
        return $this->getDriver()
            ->getQuote();
    }

    /**
     * Sets quote symbol for names(tables, columns). Which using in
     * quoteName() and qn() methods
     *
     * @param   string  $quoteForNames  Quote symbol
     *
     * @return  $this
     */
    public function setQuoteForNames( $quoteForNames )
    {
        $this->getDriver()
            ->setQuoteForNames( $quoteForNames );

        return $this;
    }

    /**
     * Returns quote symbol for names(tables, columns). Which using in
     * quoteName() and qn() methods
     *
     * @return  string
     */
    public function getQuoteForNames()
    {
        return $this->getDriver()
            ->getQuoteForNames();
    }

    /**
     * Returns driver class name
     *
     * @param   string  $driverName Driver name
     *
     * @return  string
     */
    public function getDriverClassName( $driverName )
    {
        $dbDriverClass = ucfirst( strtolower( $driverName ) ) . 'Driver';

        return $dbDriverClass;
    }

    /**
     * Returns driver result class name
     *
     * @param   string  $driverName Driver name
     *
     * @return  string
     */
    public function getDriverResultClassName( $driverName )
    {
        $dbDriverResultClass = $this->getDriverClassName( $driverName ) . 'Result';

        return $dbDriverResultClass;
    }

    /**
     * Creates db driver instance and connects to the database
     *
     * @param   string  $driverName Db driver name
     *
     * @return  bool
     */
    public function connect( $driverName )
    {
        $dbDriverClass = $this->getDriverClassName( $driverName );

        if ( ! class_exists( $dbDriverClass ) ) {
            return false;
        }

        $dbHost     = \Configuration::getParameter( 'dbHost' );
        $dbUser     = \Configuration::getParameter( 'dbUser' );
        $dbPassword = \Configuration::getParameter( 'dbPassword' );
        $dbCharset  = \Configuration::getParameter( 'dbCharset' );
        $dbName     = \Configuration::getParameter( 'dbName' );

        /** @var MysqliDriver $dbDriver */
        $dbDriver = new $dbDriverClass(
            $this,
            $dbHost,
            $dbUser,
            $dbPassword,
            $dbCharset,
            $dbName
        );

        $this->setDriver( $dbDriver );

        $dbDriver->connect();

        return true;
    }

    /**
     * Executes sql query
     *
     * @param   string  $query  Query to execute
     *
     * @return  bool|mysqli_result
     */
    public function execute( $query )
    {
        $driver = $this->getDriver();

        $resultResource = $driver->execute( $query );

        return $resultResource;
    }
    
    /**
     * Quotes and optionally escapes a string to
     * database requirements for insertion into the database
     *
     * @param   string  $text   Text to quote
     * @param   bool    $escape To escape or not
     *
     * @return  string
     */
    public function quote( $text, $escape = true )
    {
        return $this->getDriver()
            ->quote( $text, $escape );
    }

    /**
     * Wrap an SQL statement identifier name such as column,
     * table or database names in quotes
     *
     * @param   string  $name   Text to quote
     * @param   bool    $escape To escape or not
     *
     * @return  string
     */
    public function quoteName( $name, $escape = true )
    {
        return $this->getDriver()
            ->quoteName( $name, $escape );
    }

    /**
     * Alias for quote() method
     *
     * Quotes and optionally escapes a string to
     * database requirements for insertion into the database
     *
     * @param   string  $text   Text to quote
     * @param   bool    $escape To escape text or not
     *
     * @return  string
     */
    public function q( $text, $escape = true )
    {
        return $this->quote( $text, $escape );
    }

    /**
     * Alias for quoteName() method
     *
     * Wrap an SQL statement identifier name such as column,
     * table or database names in quotes
     *
     * @param   string  $name   Name to quote
     * @param   bool    $escape To escape text or not
     *
     * @return  string
     */
    public function qn( $name, $escape = true )
    {
        return $this->quoteName( $name, $escape );
    }

    /**
     * Escapes text for usage in an SQL statement.
     * Uses mysqli_real_escape_string.
     *
     * @param   string  $text   Text to escape
     *
     * @return  string
     */
    public function escape( $text )
    {
        return $this->getDriver()
            ->escapeValue( $text );
    }

    /**
     * Quotes and escapes array for insert, update queries. Returns it.
     *
     * @param   array   $sets           Array of sets, keys are column names, values are setting values.
     * @param   bool    $quoteKeys      To quote keys(with $this->_quoteForNames) of given array or not
     * @param   bool    $quoteValues    To quote values(with $this->_quote) of given array or not
     * @param   bool    $escapeValues   To escape values of given array or not before quoting
     *
     * @return  array
     */
    public function tidySets( $sets, $quoteKeys = true, $quoteValues = true,
                              $escapeValues = true )
    {
        $tidySets = array();
        foreach ( $sets as $column => $value ) {
            if ( $quoteKeys ) {
                $column = $this->qn( $column );
            }
            if ( $escapeValues ) {
                $value = $this->escape( $value );
            }
            if ( $quoteValues ) {
                $value = $this->q( $value, false );
            }
            $tidySets[$column] = $value;
        }
        return $tidySets;
    }

    /**
     * Quotes array for select query.  Returns it.
     *
     * @param   array   $columns    Array of column names, which will be selected
     *
     * @return  array
     */
    public function quoteColumns( $columns )
    {
        foreach ( $columns as $key => $column ) {
            if ( trim( $column ) == '*' ) {
                continue;
            }
            $columns[$key] = $this->qn( $column );
        }

        return $columns;
    }

}
