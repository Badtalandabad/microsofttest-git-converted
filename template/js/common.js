$(document).ready(function() {

    // better mobile hover
    if (Modernizr.touch) {
        // run the forEach on each figure element
        [].slice.call(document.querySelectorAll('a, button')).forEach(function(el, i) {
            // check if the user moves a finger
            var fingerMove = false;
            el.addEventListener('touchmove', function(e) {
                e.stopPropagation();
                fingerMove = true;
            });
            // always reset fingerMove to false on touch start
            el.addEventListener('touchstart', function(e) {
                e.stopPropagation();
                fingerMove = false;
            });
        });
    };

    // smooth scrolling to the anchors
    $(function() {
        $('.menu-list a, #btn-inquiry1, #btn-inquiry2, #logo-link').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 500);
                    return false;
                }
            }
        });
    });

    $('#navbar-toggle').on('click', function(e) {
        e.preventDefault();
    });

    $('#main-header').pinBox({
        Top : '0px',
        Container: 'body',
        ZIndex : 999,
        MinWidth: '0px',
        Events : function(e) {
            if (e.current >= 768) {
                $('#responsive-menu').css('display', 'none!important');
            }
        }
    });

    var headerHeightNumber = $('#main-header').outerHeight();
    headerHeight =  headerHeightNumber.toString() + 'px';


    $('#sticky-tabs').pinBox({
        //default 0px
        Top : headerHeight,
        //default '.container' 
        Container : '#sticky-container',
        //default 20 
        ZIndex : 20,
        //default '767px' if you disable pinBox in mobile or tablet
        MinWidth : '0px',
        //events if scrolled or window resized  
        Events : function(e){
            var headerHeight = $('#main-header').outerHeight();
            headerHeight = headerHeight.toString() + 'px';
            // e.current => current scroll top [number]
            // e.direction => scroll down or up [up,down]
            // e.width => window width [number]
            // e.active => if pinBox active [true,false]
            // e.disabled => if window width < MinWidth pinBox will disabled [true, false]
            if (e.active == true) {
                $('#sticky-tabs').css('box-shadow', '0px 2px 8px 0px rgba(0, 0, 0, 0.24)')				
            }
            if (e.current >= 3200)	{
                $('#sticky-tabs').trigger('pinBox.reload');
            }
        }
    });

    // animated counters
    $('.stats-number').counterUp({
        delay: 10,
        time: 2000
    });
    
    $(window).imagesLoaded()
    .always( function( instance ) {
        // wow animations init
        new WOW().init();
        
        // parallax init
        $.stellar({
            positionProperty: 'transform',
            hideDistantElements: false
        });
        
    });

    // set pricing tables the equal height based on the tallest one
    if (Modernizr.mq('only all and (min-width: 768px)')) {
        $('.pricing-large').addClass('large');
        $('.step-wrapper').equalHeights();

        function adjustHeight(pricingGroup) {
            $(pricingGroup + ' .options-list-wrapper').equalHeights();
            var highestHeight = parseInt($('.pricing-large ' + pricingGroup + ' .options-list-wrapper').height() + 60);
            $('.pricing-large ' + pricingGroup + ' .options-list-wrapper').height(highestHeight);
        }

        adjustHeight('.pricing-table-group1');

        // adjust prcings in the second tab panel
        var firstTrigger = false;
        $('#link-panel2').on('click', function(e) {
            if (!firstTrigger) {
                setTimeout(function() {
                    adjustHeight('.pricing-table-group2');
                }, 1000);
                firstTrigger = true;
            }
        });
    }

    if (screen.width >= '768') {
        $(window).resize(function(){
            window.setTimeout('location.reload()', 500);
        });
    }

    $('body').scrollspy({
        target: '#top-navbar',
        offset:  headerHeightNumber
    });

    $('#link-panel1').on('click', function(e) {
        $('#responsive-menu a[href*="#content-pricing2"]').attr('href', '#content-pricing');
        $('#responsive-menu a[href*="#content-advantages2"]').attr('href', '#content-advantages');
    });

    $('#link-panel2').on('click', function(e) {
        $('#responsive-menu a[href*="#content-pricing"]').attr('href', '#content-pricing2');
        $('#responsive-menu a[href*="#content-advantages"]').attr('href', '#content-advantages2');
    });

    // set nav tabs headings to the equal height
    $('.nav-tabs li').equalHeights();
    
    // set features blocks to the equal height to prevent issues on SM and XS 
    $('.section-features .feature-block').equalHeights();

    // form 1
    $('#form1').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'mail.php',
            data: $(this).serialize()
        }).done(function() {
            setTimeout(function() {
                alert('Спасибо! Мы свяжемся с вами в ближайшее время.');
            }, 1500);
        }).fail(function() {
            setTimeout(function() {
                alert('Возникла непредвиденная ошибка! Пожалуйста, свяжитесь с нами по телефону, указанному на сайте.');
            }, 1500);
        });
    });

    $('#questions-form').submit(function () {
        var form = $(this);
        var url = window.location.pathname + '?ajax=1';
        $.post(url, $('#questions-form').serialize())
            .always(function (data) {
                // form.parents('.b-question')
                //     .hide();
            })
            .done(function (data) {
                $('.b-success__recipient-email').text($('.b-question__recipient-email').val());
                $('.b-success').show();
            }).fail(function (data) {
                $('.b-failure').show();
        });
        return false;
    });

    $('.quiz-tabs').children('.quiz-tabs-tab')
        .first()
        .addClass('active');
    $('.tab-content').children('.quiz-block')
        .first()
        .addClass('active');


    $('.b-question__button').click(function () {
        var id = $(this).attr('data-id');
        var answer = $(this).attr('data-answer');

        $('input[name="answers[' + id + ']"]').val(answer);

        $(this).parents('.quiz-block')
            .removeClass('active')
            .next()
            .addClass('active');

        $('.quiz-tabs .active').removeClass('active')
            .addClass('answered')
            .next()
            .addClass('active');
    });

});